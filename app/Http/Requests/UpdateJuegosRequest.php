<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateJuegosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:4', 'max:255'],
            'url' => ['required'],
            'url_image' => ['required'],
            'status' =>['required']
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Se debe ingresar un nombre',
            'name.min' => 'El nombre debe tener minimo 4 caracteres',
            'name.max' => 'El nombre debe tener maximo 255 caracteres',
            'url.required' => 'Se debe ingresar url',
            'url_image.required' => 'Se debe ingresar una Imagen',
            'status.required' => 'Se debe ingresar un estado'   
        ];
    }

}
