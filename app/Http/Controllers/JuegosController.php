<?php

namespace App\Http\Controllers;

use App\Models\Juegos;
use App\Http\Requests\StoreJuegosRequest;
use App\Http\Requests\UpdateJuegosRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class JuegosController extends Controller
{
   
    public function index()
    {
       $juegos = Juegos::paginate(10);
       return view('juegos.index')->with('juegos', $juegos);
     
    }

   
    public function create()
    {
        return view('juegos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreJuegosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJuegosRequest $request)
    {
        Juegos::Create([
            'name' => $request->name,
            'url' =>  $request->url,
            'url_image' => $request->url_image,
            'description' => $request->description,
            'status' => $request->status
        ]);
    	
        return redirect()->route('juegos.index')->with('status', 'Juego Creado con éxito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Juegos  $juegos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $juego = Juegos::FindOrFail($id);
        return view('juegos.edit')->with('juego', $juego);
    }

    
    public function update(UpdateJuegosRequest $request, $id)
    {
        // dd($request);
        $juego = Juegos::FindOrFail($id);
        $juego->name = $request->name;
        $juego->url =  $request->url;
        $juego->url_image = $request->url_image;    
        $juego->description = $request->description;
        $juego->status = $request->status;

        $juego->save();
        return redirect()->route('juegos.index')->with('status', 'Juego Actualizado con éxito');
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Juegos  $juegos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Juegos::findOrFail($id)->delete();
        return response()->json(['msg' => 'Juego Eliminado con éxito']);
       
    }

    public function upload_image(Request $request){

        try {
            $imagen_decoded = $request->img;
            
            $ext = $request->ext;
            
            $folderPath = "img/imagenes_juegos/";
            if(!File::exists($folderPath)) {
                File::makeDirectory($folderPath, $mode = 0777, true, true);
            }
        
            $image_parts = explode(";base64,", $imagen_decoded);
            
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . uniqid() . '.'.$ext;
            if(file_put_contents($file, $image_base64)){
                $ruta_final = asset($file);
                return response()->json(["result" => true, "msg" => "Archivo cargado con éxito", "archivo" => $ruta_final]);  
            }else{
                return response()->json(["result" => false, "msg" => "Ha ocurrido un error, no se ha cargado el archivo"]);  
            }

        } catch (\Throwable $th) {
            
            return response()->json(["result" => false, "msg" => "Ha ocurrido un error, no se ha cargado el archivo", "det" => $th->getMessage()]);  
        }
               
         
        
    }
}
