
function base64MimeType(encoded) {
	var result = null;
	if (typeof encoded !== 'string') {
	  return result;
	}
	var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);
	if (mime && mime.length) {
	  result = mime[1];
	}
  
	return result;
}

$(document).on('change', '#img_file', function(){
   
    var files = $('#img_file').prop('files');
    var token = $('input[name="_token"]').val();;
    if (files && files[0]) {
		var FR= new FileReader();
		FR.addEventListener("load", function(e) {
		  var size = document.getElementById('img_file').files[0].size;
			if(size > 10000000){
				//  noty({
				// 	text: "Tamaño del archivo no puede exceder los 10 mb",
				// 	layout: 'topCenter',
				// 	type: 'error',
				// 	timeout: 6000,
				// 	killer: true
				// }); 
				return false;
			}
		  
		  var img = '';
		  img = e.target.result;
     
      
		  var cadena = base64MimeType(img); 
      let ext = cadena.split('/')[1];
     
		   if((cadena.indexOf("image") > -1) ){
			  img = img.split(',');
				// noty({
				// 		text: "Por favor, espere un momento.",
				// 		layout: "topCenter",
				// 		type: "alert",
				// 		closeWith: [],
				// 		killer:true,
				// 		template: '<div class="noty_message"><img src="/imagenes/sitio/ajax-loader.gif">&nbsp;&nbsp;<span class="noty_text"></span><div class="noty_close"></div></div>',
				// 		fondo: '<div id="fondo" style=" position: fixed; top:0; height: 100%; width:100%; background-color: rgba(60, 56, 56, 0.38); display:block;z-index: 9999;"></div>'
				// 	});  
				$.ajax({
						url: '/juegos/upload-image',
						type: 'post',
						dataType: 'json',
						data:{img:e.target.result, ext:ext},
						headers: {
                'X-CSRF-TOKEN': token
            },
						success: function (json) {
							if (json.result) {
                    $("#url_image").val(json.archivo);
                    document.getElementById("img_preview").src = e.target.result;

							//   noty({
							// 	  text: json.msg,
							// 	  layout: 'topCenter',
							// 	  type: 'success',
							// 	  timeout: 6000,
							// 	  killer: true
							//   }); 
							}
							else {
								// noty({
								// 	text: json.msg,
								// 	layout: 'topCenter',
								// 	type: 'error',
								// 	timeout: 6000,
								// 	killer: true
								// }); 
							}
						},
						error: function(j){
						  
						//   noty({
						// 			text: "El archivo que intenta subir presenta algunos problemas. Pruebe con otro archivo",
						// 			layout: 'topCenter',
						// 			type: 'error',
						// 			timeout: 6000,
						// 			killer: true
						// 		}); 
						} 
					});
			 
			  
		   }else{
			
			    // noty({
				// 	  text: 'Archivo subido no posee un formato válido (formatos aceptados: pdf, jpg, png)',
				// 	  layout: 'topCenter',
				// 	  type: 'error',
				// 	  timeout: 6000,
				// 	  killer: true
				// 	}); 
		   }

		}); 
		  FR.readAsDataURL( this.files[0] );
	  }

});


$(document).on('click', '#edit-url-image', function(){
  $('#url_image').attr('readonly', false);
});
