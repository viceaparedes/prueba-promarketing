  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left info">
          <p></p>
          <a href="#"><i class="fa fa-circle text-success"></i> </a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Contenidos</li>
        <li class="active"><a href="{{route('juegos.index')}}"><i class="fa fa-link"></i> <span>Juegos</span></a></li>
      </ul>
    </section>
   
  </aside>