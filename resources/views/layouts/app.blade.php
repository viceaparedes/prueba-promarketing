<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Prueba - Promarketing</title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('css/adminlte/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/adminlte/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('css/adminlte/Ionicons/css/ionicons.min.css')}}">
  <!-- Tema y skin -->
  <link rel="stylesheet" href="{{asset('css/adminlte/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/adminlte/skin-blue.min.css')}}">
  <!--custom css-->
  @yield('css')


  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!--Sweet Alert-->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  @include('layouts.header')
  <!-- Left side column. contains the logo and sidebar -->
  @include('layouts.sidebar')

  <div class="content-wrapper">
    <section class="content-header">
      @yield('breadcrumb')
    
    </section>

    <section class="content container-fluid">
        @yield('contenido')
    </section>
  </div>
    @include('layouts.footer')


<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery  -->
<script src="{{asset('js/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('css/adminlte/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('css/adminlte/adminlte.min.js')}}"></script>
<!--Custom js-->
@yield('js')

</body>
</html>