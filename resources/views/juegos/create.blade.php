@extends('layouts.app')
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{route('juegos.index')}}">Juegos</a></li>
    <li class="active">Crear</li>
</ol>
@endsection
@section('contenido')
<section class="content-header">
    <h1>
        Juegos
    </h1>
</section>
@if ($errors->any())
<script>
    swal("Error al crear juego!", "@php echo implode('\n',$errors->all()) @endphp", "error");

</script>
@endif

<div class="row">
    <div class="col-md-8">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Crear Nuevo Juego</h3>
            </div>
            <form role="form" action="{{route('juegos.store')}}" method="POST">
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="name" class="form-control" id="name" name="name" placeholder="Nombre">
                    </div>

                    <div class="form-group">
                        <label for="url">Url del Juego</label>
                        <input type="url" class="form-control" name="url" id="url" placeholder="Url">
                    </div>
                    <div class="form-group">
                        <label for="description">Descripción</label>
                        <textarea id="description" class="form-control" name="description" rows="10" ></textarea>
                        <div>
                            <div class="form-group">
                                <label for="img_file">Imagen del Juego</label>
                                <span>
                                    <img src="{{asset('img/no_image.jpg')}}" id="img_preview" width="400px" heigth="400px" />
                                </span>
                                <input type="file" id="img_file" name="img_file">
                                <br>
                                <label>Tambien puede ingresar la imagen de forma manual presinando <a id="edit-url-image">aqui</a></label>
                                <input type="text" name="url_image" id="url_image" value="" class="form-control" readonly>

                            </div>

                            <div class="form-group">
                                <label for="status">Estado</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>

                                </select>
                            </div>
                        </div>
                      <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Crear Juego</button>
                      </div>
            </form>
        </div>

@endsection

@section('js')
<script src="{{asset('js/Juegos/carga-imagenes.js')}}"></script>
@endsection
