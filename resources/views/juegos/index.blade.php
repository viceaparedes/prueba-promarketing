@extends('layouts.app')

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="/"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active">Juegos</a></li>

</ol>
@endsection
@section('contenido')

<section class="content-header">
    <h1>Juegos</h1>
</section>
@if(session('status'))
<script>
    swal("¡Juego Creado!", "{{session('status')}}", "success");

</script>
{{-- <h4>{{session('status')}}</h4> --}}
@endif


<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listado de Juegos</h3>
                @csrf
                <div class="box-tools">
                    <div class="input-group input-group-sm " style="width: 150px;">
                        <a type="button" href="{{route('juegos.create')}}" class="btn btn-block btn-primary">Crear Nuevo Juego</a>
                    </div>
                </div>
            </div>

            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Nombre de Juego</th>
                        <th>Url de juego</th>
                        <th>Imagen</th>
                        <th>Última modificación Registro</th>
                        <th>Estado</th>

                        <th>Opciones</th>
                    </tr>
                    @foreach ($juegos as $j )
                    <tr>
                        <td>{{$loop->iteration }}</td>
                        <td>{{$j->name}}</td>
                        <td><a href="{{$j->url}}" target="_blank">{{$j->url}}</a></td>
                        <td><button class="ver-imagen btn btn-default" data-toggle="modal" data-target="#exampleModal" rel="{{$j->url_image}}">Ver imagen</button></td>
                        <td>{{$j->updated_at->format('d-m-Y H:i:s')}}</td>
                        <td>
                            @if($j->status == 1)
                            <span class="label label-success">Activo</span>
                            @else
                            <span class="label label-danger">Inactivo</span>
                            @endif

                        </td>
                        <td>
                            <span>
                                <a href="{{route('juegos.edit', $j->id)}}"><i class="fa fa-edit"></i>Editar</a>
                            </span>
                            <a rel="{{$j->id}}" class="eliminar" type="button"><i class="fa fa-trash"></i>Eliminar</a>
                        </td>
                    </tr>
                    @endforeach


                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{$juegos->onEachSide(5)->links()}}
            </div>
        </div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Imagen de juego</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <img src="asset('img/no_image.jpg')" id="img_preview" width="400px" heigth="400px" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
    $(document).on('click', '.eliminar', function() {
        let id = $(this).attr('rel');
        var token = $('input[name="_token"]').val();

        swal("Eliminar juego", "¿Está seguro de eliminar el juego?", 'warning', {
                buttons: {
                    cancel: "Cancelar"
                    , confirmar: "Confirmar"
                },
              })
            .then((value) => {
                switch (value) {
                    case "cancel":
                        break;

                    case "confirmar":
                        $.ajax({
                            url: '/juegos/' + id,
                              type: 'DELETE',
                              dataType: 'json',
                              data: {
                                id: id
                            },
                              headers: {
                                'X-CSRF-TOKEN': token,
                              },
                              success: function(json) {
                                swal("¡Juego Eliminado!", json.msg, "warning");
                                setTimeout(function() {
                                    window.location.reload();
                                }, 2000);

                            },error: function(j) {
                                swal("¡Ha ocurrido un error!", "Ha ocurrido un error, no se ha eliminado el juego", "error");
                            }
                        });
                        break;
                }
            });
    });
</script>
<script>
$(document).on('click', '.ver-imagen', function(){
  let img = $(this).attr('rel');
  document.getElementById("img_preview").src = img;
})

</script>
@endsection
