# Proyecto de prueba promarketing

Este proyecto fue realizado para la prueba técnica de promarketing, la cual fue desarrollada principalmente con las siguientes tecnologías

- Laravel
- Jquery
- Bootstrap (AdminLTE)

## Contenido

- Programa base en Laravel
- CRUD de creación de juegos
- Subida de imagenes directa a sitio, o bien carga de enlace en input de texto


## Contenido técnico

Este proyecto fue realizado con las siguientes tecnologías:

- [Laravel](https://laravel.com/) - Para la programación del Backend
- [Jquery] - Como apoyo para creación de animaciones y funcionalidades con Javascript
- [Mysql](https://www.mysql.com/) - Como apoyo para creación de animaciones y funcionalidades con Javascript
- [Bootstrap](https://getbootstrap.com/) - Como principal estílo gráfico
- [AdminLTE](https://adminlte.io/) - Como template principal del proyecto
- [GitLab](https://gitlab.com/) - Para control de versiones
- [SweetAlert](https://sweetalert.js.org/) - Para el manejo de Notificaciones





## Installation

El sitio requiere PHP 7.3+ para su correcto funcionamiento, además de tener composer instalado.
Adicionalmente se requiere un motor de base de datos, para el desarrollo se utilizó Mysql como gestor de base de datos, pero puede usar el de su preferencia, dada la creación de migraciones existente.
Teniendo lo anterior en cuenta, debe ejecutar los siguientes comandos

```sh
cp .env.example .env
composer install
php artisan key:generate
```

Antes de ejecutar los siguientes comandos, asegurese de que tiene una base de datos creada y configurada en el archivo .env

```sh
php artisan migrate --seed
```
Finalmente debe correr el proyecto con

```sh
php artisan serve
```

## Consideraciones extra

Este instructivo esta hecho para que el proyecto corra con *php artisan serve* si desea correr la aplicación con docker o xampp, debe hacerlo según las indicaciones que le entreguen estas aplicaciones.