<?php

use App\Http\Controllers\JuegosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('juegos', JuegosController::class)->except([
    'show'
]);

Route::post('juegos/upload-image', [JuegosController::class, 'upload_image'])->name('juegos.upload-image');